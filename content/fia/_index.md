+++

title = "Fault Injection Attacks"

description = "Fault injection attacks try to disrupt the normal behavior to gain access or to obtain secret information. Fault injection can be achieved with hardware means: clock glitches, laser pulses, electromagnetic pulses, ... RowHammer is a technique where a fault is obtained with software means."

+++
