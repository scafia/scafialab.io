+++

title = "Observation attacks"

description = "Observation attacks try to exploit any indirect source of information to gain some knowledge on the data processed by the target. It can be the timing of an operation, the power consumption of the target, the emitted electromagnetic radiation, ..."

sort_by = "order"

+++
