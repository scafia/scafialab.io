+++

title = "Measuring electromagnetic leakage"
order = 3
date = 2018-01-18
draft = false


+++

In this lesson, we will see how to measure electromagnetic leakage.

## Measuring electromagnetic leakage

<img src="STM32.jpg" style="width: 100%; display: block; margin-left: auto; margin-right: auto;" >

### Principles


### Measuring

We will need apparatus for that:
- a probe, here we will work with near-field magnetic probes,
- an amplifier to be able to observe the small EM leakage,
- an oscilloscope, with a sufficiant bandwidth with respect to the target.

In our case:
<div class="mdl-grid">
  <div class="mdl-card mdl-shadow--2dp">
    <div class="mdl-card__title">
      <h2 class="mdl-card__title-text">Probe</h2>
    </div>
    <div class="mdl-card__media">
      <img src="probe.png" width="220px" style="display: block; margin-left: auto; margin-right: auto;" >
    </div>
    <div class="mdl-card__supporting-text">
      RF-U 5-2 probe from Langer.
    </div>
    <div class="mdl-card__actions mdl-card--border">
      <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="https://www.langer-emv.de/en/product/rf-passive-30-mhz-3-ghz/35/rf2-set-near-field-probes-30-mhz-up-to-3-ghz/272/rf-u-5-2-h-field-probe-30-mhz-up-to-3-ghz/16">
        Go to probe page
      </a>
    </div>
  </div>

  <div class="mdl-card mdl-shadow--2dp">
    <div class="mdl-card__title">
      <h2 class="mdl-card__title-text">Amplifier</h2>
    </div>
    <div class="mdl-card__media">
      <img src="amplifier.jpg" width="220px" style="display: block; margin-left: auto; margin-right: auto;" >
    </div>
    <div class="mdl-card__supporting-text">
      PA303, 30dB amplifier from Langer.
    </div>
    <div class="mdl-card__actions mdl-card--border">
      <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="https://www.langer-emv.de/en/product/preamplifier/37/pa-303-bnc-set-preamplifier-100-khz-up-to-3-ghz/519/pa-303-bnc-preamplifier-100-khz-up-to-3-ghz/41">
        Go to amplifier page
      </a>
    </div>
  </div>

  <div class="mdl-card mdl-shadow--2dp">
    <div class="mdl-card__title">
      <h2 class="mdl-card__title-text">Oscilloscope</h2>
    </div>
    <div class="mdl-card__media">
      <img src="oscillo.jpg" width="220px" style="display: block; margin-left: auto; margin-right: auto;" >
    </div>
    <div class="mdl-card__supporting-text">
      DSOS404A 4GHz oscilloscope from Keysight.
    </div>
    <div class="mdl-card__actions mdl-card--border">
      <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="www.keysight.com/en/pdx-x202070-pn-DSOS404A/high-definition-oscilloscope-4-ghz-4-analog-channels">
        Go to oscilloscope page
      </a>
    </div>
  </div>
</div>

The setup is expensive, and clearly overkill if targeting a microcontroller. A lower end oscilloscope is enough in this case.
To finish the setup, connect the probe to the amplifier then the amplifier output to the oscilloscope.
Connect the target to the oscilloscope for the trigger: it will indicate when to start the measurements.
Last, connect the oscilloscope to the host computer in order to command the oscilloscope synchronously with the target.

You can then program the host to launch an experiment:

```
open connection to oscilloscope
open conniction to target

for each execution
  set oscilloscope in single mode
  send 'execute' command to target
  expect target answer
  stop oscilloscope and get trace back
  store trace to disk
end for

close connection
```

To speedup measurements (since in our case TCP/IP has a high latency), we can do batch measurements using the 'segmented' mode of the oscilloscope.

In our lab, we use custom-made (and open-source) software to issue commands to apparatus: [Sparkbench](https://gitlab.com/Artefaritaj/Sparkbench).
At the end of this step we have files containing the wanted data: the traces, and the metadata (e.g. plaintexts for cryptographic applications).