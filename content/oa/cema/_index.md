+++

title = "Correlation Electromagnetic Analysis"
description = "In this chapter, we will use Correlation Electromagnetic Analysis (CEMA) to analyze the electromagnetic radiation of the target in order to find a secret cryptographic key (AES-128)."
sort_by = "order"
order = 2

[extra]
commons = ["common/STM32F1_target/index.md", "common/aes.md"]

+++