+++

title = "Target application: AES encryption"
date = 2018-02-09

draft = false

[extra]
latex = true

+++

The Advanced Encryption Standard (AES) is a symetric encryption standard [defined by the NIST](http://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.197.pdf) in 2001.

## AES

### Definitions

The AES is a block cipher, working on 128-bit plaintexts, with 3 possible key sizes: 128, 192 or 256 bits. In this lesson, we will describe only the 128 bits variant.
The ciphering algorithm works on a State: a 4 by 4 bytes matrix, initialized with the plaintext value.
This State will undergo transformations in rounds (10 rounds for AES-128). At each round, 4 State transformations are applied.

### State transformations

#### AddRoundKey

The AddRoundKey transformation produce a new State by xoring the State with the round key: the key uniquely derived from the master key for this specific round.
See, the key scheduling section to see how to derive the round key.

#### SubBytes

SubBytes is the substitution layer: for each byte of the State, an s-box is applied. An s-box is a non linear permutation: each byte value is mapped to another one in a bijective manner.

#### ShiftRows

As the name tell us, rows of the State are shifted in this operation, at the byte level. The first row is kept intact, the second one is shifted by one byte to the left, the third row is shifted by two bytes to the left and the last row is shifted by three bytes to the left.

#### MixColumns

The MixColums is a matrix multiplication (thus a linear transformation) applied on each column of the State. See the [doc](http://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.197.pdf) for more details.

### Key Scheduling

The key scheduling (called in the case of the AES Key Expansion) is the algorithm to derive round keys from the master key. The key flow is arranged as a matrix with 4 rows: the master key is seen as a 4 by 4 byte matrix. The fifth column (first column of the second round key) is derived from the first and the fourth column. Each new column can be computed from the previous ones, so that each round key can be computed from the previous round key (and the round number).

### Modes of operation

The mode of operation define how blocks of plaintext are handled: in the Electronic CodeBook (ECB) mode, for each input block, the output block is simply the result of AES(input).
The problem is that patterns of data split in several blocks can still be seen after the encryption. The Cipher Block Chaining (CBC) mode mitigate this problem with a more complex scheme:
we define a initial vector (IV) of 128 bits. The the first output block \\(o_1\\) is defined as \\(o_1 = AES(i_1 \oplus IV)\\) then \\(\forall i > 1, o_i = AES(i_i \oplus o_{i-1})\\).

The CBC mode still leaks information in the frequency domain, as of the time of this writing the preferred mode is AES-GCM.