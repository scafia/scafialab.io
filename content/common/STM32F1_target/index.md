+++
title = "Target device: STM32VLDISCOVERY" 
order = 2
date = 2018-01-12
+++

In this lesson, we will see how to setup and program the [STM32VLDISCOVERY](http://www.st.com/en/evaluation-tools/stm32vldiscovery.html) board that we will use as a target.

<div class="mdl-grid">
  <div class="mdl-card mdl-shadow--2dp">
    <div class="mdl-card__title">
      <h2 class="mdl-card__title-text">The board</h2>
    </div>
    <div class="mdl-card__media">
      <img src="board.jpg" width="220px" style="display: block; margin-left: auto; margin-right: auto;" >
    </div>
    <div class="mdl-card__supporting-text">
      The STM32VLDISCOVERY board from STMicroelectronics.
    </div>
    <div class="mdl-card__actions mdl-card--border">
      <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="http://www.st.com/en/evaluation-tools/stm32vldiscovery.html">
        Go to board page
      </a>
    </div>
  </div>

  <div class="mdl-card mdl-shadow--2dp">
    <div class="mdl-card__title">
      <h2 class="mdl-card__title-text">FTDI Friend: Serial-to-USB adapter</h2>
    </div>
    <div class="mdl-card__media">
      <img src="ftdifriend.jpg" width="220px" style="display: block; margin-left: auto; margin-right: auto;" >
    </div>
    <div class="mdl-card__supporting-text">
      The FTDI Friend from Adafruit. But you can use any Serial-to-USB adapter.
    </div>
    <div class="mdl-card__actions mdl-card--border">
      <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="https://www.adafruit.com/product/284">
        Go to board page
      </a>
    </div>
  </div>
</div>

### Board presentation

The targeted board is the STM32VLDISCOVERY from STMicroelectronics. This is a low cost board (~10€) with a Cortex-M3 based microcontroller (STM32F1).

This board is powered through USB, the same connection is used for debugging and programming (put the binary in the chip).
Communications with the host are done with a UART connection. For that we will need a UART to USB adapter.

### Serial-over-USB communications

Serial communication (UART) can be used with the use of the dedicated USB mode of operation.
We use a dedicated board from Adafruit, the [FTDIFriend](https://www.adafruit.com/product/284) to interface the STM32 board to the host computer: the STM32 board will communicate with the FTDIFriend with UART, and the FTDIFriend is connected with a USB cable to the host computer.
From the host perspective, we will see a serial interface in the form of a */dev/ttyUSBx* file: writing or reading to this file allows to communicate with the board.
For better controls, the GtkTerm terminal can be used. To get the correct rights, add your user to the dialout group. Under Ubuntu:

```bash 
sudo apt-get install gtkterm
sudo adduser $USERNAME dialout
```

### Programming environment setup

#### Build a project

Our microcontroller is programmend in C, and use the chip dedicated libraries provided by ST under the marketed name of [STM32CubeF1](http://www.st.com/en/embedded-software/stm32cubef1.html). These libraries allow us to interact easily with all the chip peripherals (clock managemnent, UART, GPIO...).

First, clone the basic AES application:

```bash
git clone https://gitlab.com/scafia/stm32vldiscovery_aes.git
```

You can explore the repository, it consists in a basic AES encryption application: plaintexts are sent to the STM32 by the host they are encrypted and the ciphertexts are sent back to the host.

To build the binary, a makefile is provided but first you have to install the toolchain: the set of compiler, linker and other tools that target this particular chip (ARMv7-M Instruction set). With Ubuntu, the toolchain is just a
```bash
sudo apt-get install gcc-arm-none-eabi gdb-arm-none-eabi
```
away.

With the toolchain present, build your project:
```bash
cd stm32vldiscovery_aes
make
```

The *make* command produce a *elf* executable file. If necessary (in our case it is not), you can obtain the raw binary with
```bash
arm-none-eabi-objcopy -O binary aes.elf aes.bin
```

In our case, the *elf* file is properly handled by our programming tool, OpenOCD.

#### Programming the STM32

Programming is the action of copying the binary into the microcontroller memory for it to be executed. We will use [OpenOCD](http://openocd.org/) as our programming tool: it deals with a lot of different board and a configuration file for our own board is provided. The connection will use the USB connection with the STM32, the UART connection is not needed for that.

```bash
sudo apt-get install openocd
sudo openocd -f /usr/local/share/openocd/scripts/board/stm32vldiscovery.cfg
```

The location of the configuration file may move according to openocd version. If it does not find it, locate it with
```bash
locate stm32vldiscovery.cfg
```

There is a bug with this particular board and Ubuntu, where Ubuntu see the board as a mass storage and forbid the OpenOCD connection.
If it occurs, use the provided script file *Docs/launch_ocd.sh* to launch OpenOCD.

Once the OpenOCD server is launched in a terminal, open a second one then start a telnet connection to the OpenOCD server.

```bash
telnet localhost 4444
```

then

```bash
program aes.elf
```

When programing is done, reset the chip to launch the newly loaded program:

```bash
reset run
```

#### Exploring how the project work

Lets open the *src/main.c* file to explore how this project work.

Initialization:
```C
/* STM32F1xx HAL library initialization:
       - Configure the Flash prefetch
       - Systick timer is configured by default as source of time base, but user
         can eventually implement his proper time base source (a general purpose
         timer for example or other time source), keeping in mind that Time base
         duration should be kept 1ms since PPP_TIMEOUT_VALUEs are defined and
         handled in milliseconds basis.
       - Set NVIC Group Priority to 4
       - Low Level Initialization
    */
HAL_Init();

/* Configure the system clock to 24 MHz */
SystemClock_Config();


/* -1- Enable each GPIO Clock (to be able to program the configuration registers) */
LED3_GPIO_CLK_ENABLE();
LED4_GPIO_CLK_ENABLE();
__HAL_RCC_GPIOC_CLK_ENABLE();

/* -2- Configure IOs in output push-pull mode to drive external LEDs */
GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
GPIO_InitStruct.Pull  = GPIO_PULLUP;
GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;

GPIO_InitStruct.Pin = LED3_PIN;
HAL_GPIO_Init(LED3_GPIO_PORT, &GPIO_InitStruct);

GPIO_InitStruct.Pin = LED4_PIN;
HAL_GPIO_Init(LED4_GPIO_PORT, &GPIO_InitStruct);

GPIO_InitStruct.Pull  = GPIO_NOPULL;
GPIO_InitStruct.Pin = GPIO_PIN_6;
HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

/*##-1- Configure the UART peripheral ######################################*/
/* Put the USART peripheral in the Asynchronous mode (UART Mode) */
/* UART configured as follows:
    - Word Length = 8 Bits (8 data bit + 0 parity bit)
    - Stop Bit    = One Stop bit
    - Parity      = ODD parity
    - BaudRate    = 9600 baud
    - Hardware flow control disabled (RTS and CTS signals) */
UartHandle.Instance        = USARTx;

UartHandle.Init.BaudRate   = 115200;
UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
UartHandle.Init.StopBits   = UART_STOPBITS_1;
UartHandle.Init.Parity     = UART_PARITY_NONE;
UartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
UartHandle.Init.Mode       = UART_MODE_TX_RX;

if(HAL_UART_DeInit(&UartHandle) != HAL_OK)
{
  Error_Handler();
}
if (HAL_UART_Init(&UartHandle) != HAL_OK)
{
  /* Initialization Error */
  Error_Handler();
}

//Send welcoming message
HAL_UART_Transmit(&UartHandle, (uint8_t *)welcome, strlen(welcome), 0xFFFF);
```
and
```C
/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow :
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 24000000
  *            HCLK(Hz)                       = 24000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 1
  *            APB2 Prescaler                 = 1
  *            HSE Frequency(Hz)              = 8000000
  *            HSE PREDIV1                    = 2
  *            PLLMUL                         = 6
  *            Flash Latency(WS)              = 0
  * @param  None
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef clkinitstruct = {0};
  RCC_OscInitTypeDef oscinitstruct = {0};

  /* Enable HSE Oscillator and activate PLL with HSE as source */
  oscinitstruct.OscillatorType  = RCC_OSCILLATORTYPE_HSE;
  oscinitstruct.HSEState        = RCC_HSE_ON;
  oscinitstruct.HSEPredivValue  = RCC_HSE_PREDIV_DIV2;
  oscinitstruct.PLL.PLLState    = RCC_PLL_ON;
  oscinitstruct.PLL.PLLSource   = RCC_PLLSOURCE_HSE;
  oscinitstruct.PLL.PLLMUL      = RCC_PLL_MUL6;
  if (HAL_RCC_OscConfig(&oscinitstruct)!= HAL_OK)
  {
    /* Initialization Error */
    while(1);
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
     clocks dividers */
  clkinitstruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  clkinitstruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  clkinitstruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  clkinitstruct.APB2CLKDivider = RCC_HCLK_DIV1;
  clkinitstruct.APB1CLKDivider = RCC_HCLK_DIV1;
  if (HAL_RCC_ClockConfig(&clkinitstruct, FLASH_LATENCY_0)!= HAL_OK)
  {
    /* Initialization Error */
    while(1);
  }
}
```

In the initialization, the required peripherals are setup: the clock, the LEDs, a GPIO (PC6) to use as a trigger for our measurements and the UART connection (115200 bauds, 8N1).
At the end, we send a welcoming message to signal to the host that we are ready to accept commands.


Next in an infinite loop, we accept a 1 character command and deal with it accordingly:

```C
while (1)
  {
    //read command
    if(HAL_UART_Receive(&UartHandle, &command[0], 1, 0xFFFF) == HAL_OK)
    {
      HAL_GPIO_TogglePin(LED3_GPIO_PORT, LED3_PIN);
      switch (command[0]) {
        case 't'://test
          sprintf(text, "STM32 AES Test OK\n");
          HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);
          break;

        case 'k'://set key (binary)
          HAL_UART_Receive(&UartHandle, (uint8_t *)key, DATA_SIZE, 0xFFFF);
          if(verbose)
          {
            sprintf(text, "Key set.\n");
            HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);
          }
          break;

        //...

        case 'f'://fast mode
          HAL_UART_Receive(&UartHandle, (uint8_t *)plaintext, DATA_SIZE, 0xFFFF);//receive Plaintext
          AESEncrypt(ciphertext, plaintext, key);//AES
          HAL_UART_Transmit(&UartHandle, (uint8_t *)ciphertext, DATA_SIZE, 0xFFFF);//transmit ciphertext
          break;

        default:
          sprintf(text, "Unknown command: %c\n", command[0]);
          HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);
          break;
      }

    }
    else {
    }
  }
```

To test the application:
- build the application
- program the STM32
- connect the serial connection to the host 
- open GtkTerm with the correct parameters: /dev/ttyUSB0, 115200 bauds, 8 data bits, no parity, 1 stop bits, no flow control.
- reset the board (plug and unplug the USB connection on the STM32 board), you should observe the welcome message.
- press 't' to test the connection.

If all is well, you are ready for the next lesson.